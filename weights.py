import sys
from ultralytics import YOLO

#Download the weight file
model = YOLO("yolov9c.pt")
# if TensorRT is passes odel will convert the weight
if "tensorRT" in sys.argv:
    
    model.export(
        format="engine",
        dynamic=True,
        batch=1,
        workspace=4,
        half=True,
        int8=False,
        data="coco.yaml",
    )
