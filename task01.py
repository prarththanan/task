import cv2
from ultralytics import YOLO
import time
import os
import argparse

classes = [0, 1, 2, 3, 4, 5, 6, 7, 8]

def process_video(video_path, model, output_dir):
    """
    Process a video file using a pre-loaded YOLO model.

    Parameters:
    - video_path: str, the path to the video file
    - model: YOLO, the pre-loaded YOLO model object
    - output_dir: str, the directory to save the annotated video
    """
    video_file = os.path.basename(video_path)
    output_filename = os.path.join(output_dir, f"{os.path.splitext(video_file)[0]}_annotated.avi")

    # Open the video file
    cap = cv2.VideoCapture(video_path)
    if not cap.isOpened():
        print("Error opening video file")
        return

    # Define video writer for saving the output
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    out = cv2.VideoWriter(output_filename, fourcc, fps, (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))

    total_frames = 0
    start_time = time.time()

    while cap.isOpened():
        success, frame = cap.read()
        if not success:
            break

        total_frames += 1  # Count frames
        results = model(frame, classes=classes, conf=0.3, verbose=False)  # Run YOLO inference on the frame
        annotated_frame = results[0].plot()
        out.write(annotated_frame)  # Write the annotated frame to the output video

    # Release resources
    cap.release()
    out.release()
    cv2.destroyAllWindows()

    # Print processing results
    processing_time = time.time() - start_time
    print(f"Total Frames Processed: {total_frames}")
    print(f"Processing Time: {processing_time:.2f} seconds")
    print(f"Frames per second: {total_frames / processing_time:.2f}")

def main(model_path, file_path, output_dir):
    if not output_dir:
        output_dir = os.path.join(os.getcwd(), 'AnnotatedVideos')
    os.makedirs(output_dir, exist_ok=True)

    model = YOLO(model_path)  # Load the YOLO model

    # Handle single or multiple videos
    if os.path.isdir(file_path):
        for i in os.listdir(file_path):
            if i.endswith(('.mp4', '.avi')):
                video_path = os.path.join(file_path, i)
                print("Processing video - ",i)
                process_video(video_path, model, output_dir)
                print("Processing completed")
    elif os.path.isfile(file_path):
        print("Processing video - ",os.path.basename(file_path))
        process_video(file_path, model, output_dir)
        print("Processing completed")
    else:
        print("The provided path does not exist or is not supported.")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process videos using YOLO model')
    parser.add_argument('model_path', type=str, help='Path to the YOLO model file')
    parser.add_argument('file_path', type=str, help='Path to the video file or directory containing videos')
    parser.add_argument('--output-dir', type=str, default=None, help='Directory to save the annotated videos')

    args = parser.parse_args()
    main(args.model_path, args.file_path, args.output_dir)
