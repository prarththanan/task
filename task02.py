import os
import cv2
import time
import argparse
from ultralytics import YOLO
import concurrent.futures
import psutil
import GPUtil

process = psutil.Process(os.getpid())

def monitor_resources():
    initial_mem = process.memory_info().rss / (1024 ** 2)  # Convert bytes to MB
    gpu_initial = GPUtil.getGPUs()[0].memoryUsed if GPUtil.getGPUs() else 0
    return initial_mem, gpu_initial

initial_mem, gpu_initial = monitor_resources()
print("Initial resources:", initial_mem, gpu_initial)
classes = [0, 1, 2, 3, 4, 5, 6, 7, 8]
for_csv = []

def process_video(video_path, model, output_dir):
    print(f"Processing {video_path}")
    video_file = os.path.basename(video_path)
    output_filename = os.path.join(output_dir, f"{os.path.splitext(video_file)[0]}_processed.avi")

    cap = cv2.VideoCapture(video_path)
    if not cap.isOpened():
        print("Error opening video file")
        return

    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    out = cv2.VideoWriter(output_filename, fourcc, fps, (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))

    total_frames = 0
    start_time = time.time()

    while cap.isOpened():
        success, frame = cap.read()
        if not success:
            break

        total_frames += 1
        results = model(frame, device=0, classes=classes, conf=0.3, verbose=False)
        annotated_frame = results[0].plot()
        out.write(annotated_frame)

    cap.release()
    out.release()
    cv2.destroyAllWindows()

    processing_time = time.time() - start_time
    fps_result = total_frames / processing_time

    print(f"Processed {total_frames} frames in {processing_time:.2f} seconds ({fps_result:.2f} FPS) for {video_path}")
    mem_f, gpu_f = monitor_resources()
    return os.path.basename(video_path), total_frames, round(processing_time, 2), fps_result, mem_f-initial_mem, gpu_f-gpu_initial

def list_video_files(directory):
    extensions = ['.mp4', '.avi', '.mov']  # Add other video formats as needed
    return [os.path.join(directory, f) for f in os.listdir(directory) if os.path.splitext(f)[1].lower() in extensions]

def main(model_path, video_directory, output_directory):
    if not output_directory:
        output_directory = os.path.join(os.getcwd(), 'annotated')
    os.makedirs(output_directory, exist_ok=True)

    video_paths = list_video_files(video_directory)

    model = YOLO(model_path, task='detect')

    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
        results = executor.map(lambda x: process_video(x, model, output_directory), video_paths[:4])

    for i in range(0, len(video_paths), 4):
        batch = video_paths[i:i+4]

        with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
            results = executor.map(lambda x: process_video(x, model, output_directory), batch)
            for result in results:
                print("results : ", result)
                for_csv.append(result)

        final_resources = monitor_resources()
        print("Final resources after processing batch:", final_resources)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process videos with YOLO model.")
    parser.add_argument('--model-path', required=True, help="Path to the YOLO model file.")
    parser.add_argument('--video-directory', required=True, help="Path to the directory containing videos.")
    parser.add_argument('--output-directory', help="Path to the directory to save processed videos. If not provided, 'annotated' folder will be created in the current directory.")

    args = parser.parse_args()
    main(args.model_path, args.video_directory, args.output_directory)
