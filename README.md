# Object Detection Pipeline Project



## Overview
This project is designed to implement and evaluate object detection pipelines under various constraints. It consists of three main scripts:

- `weights.py`: Handles downloading and preparing model weights.
- `task01.py`: Processes videos one by one, detecting objects and generating annotated videos.
- `task03.py`: Adaptation of the pipeline to run four videos in parallel within specific system constraints.

### Task 01 - Build an Object Detection Pipeline
- **Objective**: Process videos sequentially to detect persons and vehicles and generate annotated videos. 
- **Approach**: Used YOLOv9c from Ultralytics to do the detections 

### Task 02 - Constrained Device Operation
- **Constraints**: 8GB GPU VRAM, 4GB system memory, 20% max CPU usage.
- **Objective**: Process four videos in parallel, maintaining the detection performance within the given constraints.
- **Approach**: Converted the model to TensorRT engine and used parallel processing to handle 4 videos simultaneously.  

## Setup and Running Instructions

### Prerequisites
- Python 3.8 or newer
- Dependencies: `pip install -r requirements.txt` 

### weights.py
This script handles the download of the YOLOv9c model weights and optionally converts the model to a TensorRT engine for optimized performance.
- To download weights only
```
python weights.py
```
- Convert the weight to TensorRT engine
```
python weights.py TensorRT
```

### task01.py
- This script is to run the Task 01. Inference of YOLOv9c and save the annotated video files.
 
```
python task01.py <path-to-model> <path-to-Video Folder/ Video File> <optional- path-to-save-directory>
```

### task02.py
- This script is to run optimized video processing pipeline to process 4 videos simultanesouly on a constraint device (8GB of GPU VRAM, 4 GB of Memory, Max CPU usage 20%). Make sure you have created tensorRT engine to run. 

```
python task02.py <path-to-TensorRT-file> <path-to-Video Folder> <optional- path-to-save-directory>
```

